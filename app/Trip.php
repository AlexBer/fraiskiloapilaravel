<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Car;

class Trip extends Model
{
  use SoftDeletes;
  protected $fillable = [
      'id', 'start', 'end', 'distance', 'date', 'roundtrip', 'compensation',
      'user_id', 'car_id'
  ];
  public function car()
    {
        return $this->belongsTo(Car::class);
    }
}
