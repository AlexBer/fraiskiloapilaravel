<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
    // Route::get('/gettrips', 'TripsController@index')->name('gettrips');
});
Route::group(['prefix' => '1.0'], function () {

    Route::get('/gettrips', 'TripsController@index')->name('gettrips');
    Route::post('/trip', 'TripsController@store')->name('addtrip');
    Route::delete('/trip/{id}', 'TripsController@destroy')->name('deletetrip');
    Route::get('/trip/{id}', 'TripsController@edit')->name('edittrip');
    Route::put('/trip/{id}', 'TripsController@update')->name('updatetrip');

    // CarsController
    Route::get('/getcars', 'CarsController@index')->name('getcars');
    Route::post('/car', 'CarsController@store')->name('addcar');
    Route::delete('/car/{id}', 'CarsController@destroy')->name('deletecar');
    Route::get('car/{id}', 'CarsController@edit')->name('editcar');
    Route::put('car/{id}', 'CarsController@update')->name('updatecar');

});

